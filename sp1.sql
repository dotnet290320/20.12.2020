create or replace FUNCTION hello_world() RETURNS varchar
language plpgsql AS
    $$
        BEGIN
            RETURN CONCAT('Hello ','World! ', ' ', current_timestamp);
        END;
    $$;

SELECT HELLO_WORLD greeting FROM hello_world();

create or replace FUNCTION a_sp_sum_of_numbers(m double precision, n double precision) returns double precision
language plpgsql AS
    $$
        DECLARE
            x integer := 1;
        BEGIN
            RETURN n + m + x;
        END;
    $$;

select * from a_sp_sum_of_numbers(3.5, 6.77777);

create or replace FUNCTION a_sum_n_product(x int, y int, OUT prod int) AS $$
    DECLARE
        sum integer := 0;
BEGIN
 sum := x + y;
 prod := x * y;
END;
$$ LANGUAGE plpgsql;

select * from a_sum_n_product(y => 10, x => 20);

create or replace function a_sp_get_movie_price(
    out min_price double precision,
    out max_price double precision,
    out avg_price double precision)
language plpgsql
as $$
begin
  select min(price),
         max(price),
		 avg(price)::numeric(5,2)
  into min_price, max_price, avg_price
  from movies;
end;$$

select * from a_sp_get_movie_price()

-- stored procedure crete or replace --> return using out the name of the most expensive movie
--            1. declare double for most expensive movie -- find the price using select ... into
--            2. use select into ... to populate the movie name (limit 1)

-- stored procuedre call it: count_sum_records which returns the count of records from movies + count of records from country
-- no args

CREATE OR REPLACE FUNCTION a_sp_most_expensive_movie(out movie_name text)
        language plpgsql AS
    $$
    DECLARE
        max_price double precision :=0;
    BEGIN
        SELECT max(price)
        into max_price
        from movies;

        SELECT movies.title
        into movie_name
        from movies where movies.price = max_price;
    END;
    $$;

CREATE OR REPLACE FUNCTION a_sp_count_sum_records() returns bigint
language plpgsql AS
    $$
    DECLARE
        MOVIES_COUNT bigint :=0;
        COUNTRY_COUNT bigint :=0;
    BEGIN
        SELECT COUNT(*) INTO MOVIES_COUNT
        FROM movies;

        SELECT COUNT(*) INTO COUNTRY_COUNT
        FROM country;

        return COUNTRY_COUNT + MOVIES_COUNT;
    END;
    $$;

select * from a_sp_count_sum_records();

CREATE OR REPLACE FUNCTION a_sp_insert_movie(_title text, _release_date timestamp, _price double precision) returns bigint
language plpgsql AS
    $$
    DECLARE
        new_id bigint;
    BEGIN
        INSERT INTO movies (title, release_date, price)
        VALUES (_title, _release_date, _price)
        returning id into new_id;

        return new_id;
    END;
    $$;

select * from  a_sp_insert_movie('Superman returns', cast('2021-05-09 22:00:00' as timestamp), 75.4);
select * from  a_sp_insert_movie('Queens gambit', cast('2022-03-10 21:21:33' as timestamp), 175.4);
call  a_sp_update_movie(7, 'Queens gambit', cast('2022-03-10 21:21:33' as timestamp), 175.4, 2);
select * from movies order by id;

-- create update function sp --> procedure

-- works also with update
--update movies set country_id = 2
--where country_id=2
--returning id

CREATE OR REPLACE PROCEDURE a_sp_update_movie(_id bigint, _title text, _release_date timestamp, _price double precision,
_country_id bigint)
language plpgsql AS
    $$
    BEGIN
        UPDATE movies
        SET title = _title, release_date = _release_date, price = _price, country_id = _country_id
        where id = _id;
    END;
    $$;

call a_sp_update_movie(1, 'batman returns', '2020-12-16 20:21:30.500000', 19.5, 1)

CREATE OR REPLACE FUNCTION a_sp_get_movies_in_range(min_price double precision, max_price double precision)
returns TABLE(id bigint, title text, release_date timestamp, price double precision, country_name text) AS
    $$
    BEGIN
        RETURN QUERY
        SELECT m.id, m.title, m.release_date, m.price, c.name FROM movies m
        join country c on m.country_id = c.id
        WHERE m.price between min_price and max_price;
    END;
$$ LANGUAGE plpgsql;

select * from a_sp_get_movies_in_range(35.0::double precision, 80.0::double precision)

select * from movies order by id;

-- return movies not the most cheap and not the most expensive!
CREATE OR REPLACE FUNCTION a_sp_get_movies_mid()
returns TABLE(id bigint, title text, release_date timestamp, price double precision, country_name text) AS
    $$
    BEGIN
        RETURN QUERY
        WITH cheapest_movie AS
            (
                select * from movies
                where movies.price = (select min(movies.price) from movies)
            ),
        expansive_movie AS
            (
                select * from movies
                where movies.price = (select max(movies.price) from movies)
            )
        SELECT m.id, m.title, m.release_date, m.price, c.name FROM movies m
        join country c on m.country_id = c.id
        WHERE m.id <> (select cheapest_movie.id from cheapest_movie) and m.id <> (select expansive_movie.id from expansive_movie);
    END;
$$ LANGUAGE plpgsql;

select * from a_sp_get_movies_mid()

-- Homework!
-- use WITH and return all movies which costs more than average and it's not the last movie (timestamp)




